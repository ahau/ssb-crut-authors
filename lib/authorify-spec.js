const isValidAuthor = require('./is-valid-author')
const authorsStrategy = require('../authors-strategy')

module.exports = function authorifySpec (spec) {
  if (spec.props && 'authors' in spec.props) {
    throw new Error('ssb-crut-authors does not allow spec.props.authors')
  }
  if (spec.staticProps && 'authors' in spec.staticProps) {
    throw new Error('ssb-crut-authors does not allow spec.staticProps.authors')
  }

  return {
    ...spec,
    props: {
      ...spec.props,
      authors: authorsStrategy // add authors prop stategy
    },
    isValidNextStep: buildIsValidNextStep(spec) // extend the validator
  }
}

function buildIsValidNextStep (spec) {
  if (!spec.isValidNextStep) return isValidAuthorStep

  return function isValidNextStep (context, node) {
    isValidNextStep.error = null
    if (!isValidAuthorStep(context, node)) {
      isValidNextStep.error = isValidAuthorStep.error
      return false
    }
    if (!spec.isValidNextStep(context, node)) {
      isValidNextStep.error = spec.isValidNextStep.error
      if (!isValidAuthorStep.error) throw new Error('here')
      return false
    }
    return true
  }
}

function isValidAuthorStep (context, node) { // TODO: move this out
  isValidAuthorStep.error = null

  // if is update
  if (node.previous !== null) {
    // check if the author of this message is in the current authors
    if (!isValidAuthor(context, node.author, node.sequence)) {
      isValidAuthorStep.error = isValidAuthor.error
      return false
    }
  }

  if (!isValidAuthorChange(context, node)) {
    isValidAuthorStep.error = node.previous === null
      ? new Error('Invalid initial authors')
      : new Error('Invalid authors change')
    return false
  }

  return true
}

function isValidAuthorChange (context, node) {
  // ensure the resulting state will have at least one active author
  const newAuthorsT = (context.tips.length === 1)
    ? authorsStrategy.concat(context.tips[0].T.authors, node.data.authors)
    : authorsStrategy.merge(context.graph, node, 'authors')

  return hasActiveAuthor(newAuthorsT)
}

function hasActiveAuthor (authorsT) {
  const authors = authorsStrategy.mapToOutput(authorsT)

  return Object.values(authors).some(authorIntervals => {
    return authorIntervals.some(({ end }) => end === null)
  })
}
