const pull = require('pull-stream')
const { isFeedId } = require('ssb-ref')

// this takes an Array of authors: [ Author ]
// and maps it to: [{ id: Author, seq }]
//   - if Author is a FeedId, seq is the current know sequence of that author
//   - if Author is '*', seq is Date.now() (current Unix Time)

module.exports = function ProcessAuthors (ssb) {
  return function processAuthors (authors, cb) {
    if (authors === undefined) return cb(null)

    if (Array.isArray(authors)) {
      if (authors.length === 0) return cb(null)
    } // eslint-disable-line
    else return cb(new Error('cannot update authors, malformed input ' + authors))

    // assign sequences to each feedId on both the add + removes

    pull(
      pull.values(authors),
      pull.asyncMap((authorFeedId, cb) => {
        if (authorFeedId === '*') return cb(null, { id: '*', seq: Number(Date.now()) })
        if (!isFeedId(authorFeedId)) return cb(new Error(`malformed feedId on authors ${authorFeedId}`))

        ssb.getFeedState(authorFeedId, (err, { sequence }) => {
          if (err) return cb(err)

          cb(null, { id: authorFeedId, seq: sequence || 0 })
        })
      }),
      pull.collect((err, result) => {
        if (err) return cb(err)

        cb(null, result)
      })
    )
  }
}
