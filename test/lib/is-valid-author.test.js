const test = require('tape')
const isValidAuthor = require('../../lib/is-valid-author')

// context: {
//   root: Msg,
//   graph: TangleGraph,
//   tips: [{ key: MsgId, T }]
// }
// author: FeedId | *
// seq: Integer | UnixTime

test('lib/isValidAuthor (single tip)', t => {
  const context = {
    root: {
      value: { author: 'alice' }
    },
    // graph
    tips: [{
      T: {
        authors: {
          alice: { 50: 1, 200: -1, 400: 1 }, // authorship from 50-200 inclusive, 400...
          charles: { 100: 1 }
        }
      }
    }]
  }

  /* pass - alice in bounds */
  t.true(isValidAuthor(context, 'alice', 50))
  t.true(isValidAuthor(context, 'alice', 200))
  t.true(isValidAuthor(context, 'alice', 450))
  /* fail - alice out of bound */
  t.false(isValidAuthor(context, 'alice', 49))
  t.false(isValidAuthor(context, 'alice', 201))

  /* fail - a stranger */
  t.false(isValidAuthor(context, 'bob', 50))

  /* pass - add author * */
  context.tips[0].T.authors['*'] = {}
  context.tips[0].T.authors['*'][Number(Date.now())] = 1 // add anyone can author
  t.true(isValidAuthor(context, 'bob', 50))

  /* fail - after remove author * */
  context.tips[0].T.authors['*'][Number(Date.now())] = -1 // remove anyone can author
  t.false(isValidAuthor(context, 'bob', 50), 'after remove author')

  /* WIPE AUTHORS */
  // this should never happen, but we want to make sure if it does that
  // the original author can always edit
  context.tips[0].T.authors = {}
  t.true(isValidAuthor(context, 'alice', 40))
  t.false(isValidAuthor(context, 'bob', 40))
  t.end()
})

test('lib/isValidAuthor (multiple tips)', t => {
  const context = {
    root: {
      value: { author: 'alice' }
    },
    // graph
    tips: [
      {
        T: {
          authors: { alice: { 50: 1 }, bob: { 100: 1 } }
        }
      },
      {
        T: {
          authors: { bob: { 100: 1 } }
        }
      }
    ]
  }

  t.false(isValidAuthor(context, 'alice', 50), 'alice not active on any tips')

  t.true(isValidAuthor(context, 'bob', 130), 'bob active on both tips')
  t.false(isValidAuthor(context, 'bob', 10))

  /* pass - add author * */
  context.tips[1].T.authors['*'] = {}
  context.tips[1].T.authors['*'][Number(Date.now())] = 1 // add anyone can author
  t.true(isValidAuthor(context, 'alice', 50))

  /* fail - after remove author * */
  context.tips[1].T.authors['*'][Number(Date.now())] = -1 // remove anyone can author
  t.false(isValidAuthor(context, 'alice', 50))

  /* edge case (A) */
  context.tips = [
    {
      T: {
        authors: { alice: { 50: 1 } }
      }
    },
    {
      T: {
        authors: { bob: { 100: 1 } }
      }
    }
  ]
  t.true(isValidAuthor(context, 'alice', 100), 'special rule: OG author can publish if lock-out')
  t.true(isValidAuthor(context, 'alice', 40), 'any seq passes!')
  t.false(isValidAuthor(context, 'bob', 100), 'non-OG author cant')
  t.end()
})
