const Crut = require('ssb-crut')
const { promisify } = require('util')

const authorifySpec = require('./lib/authorify-spec')
const ProcessAuthors = require('./lib/process-authors')

module.exports = class CrutAuthors extends Crut {
  constructor (ssb, spec, opts) {
    /* classic crut */
    super(ssb, authorifySpec(spec), opts)

    /* common helpers */
    this.processAuthors = ProcessAuthors(ssb)
  }

  create (input, cb) {
    if (cb === undefined) return promisify(this.create).call(this, input)

    const { add } = (input.authors || {})
    if (!add || !add.length) return cb(new Error('Invalid initial authors'))

    this.processAuthors(add, (err, addAuthors) => {
      if (err) return cb(err)

      super.create({ ...input, authors: { add: addAuthors } }, cb)
    })
  }

  update (id, input, cb) {
    if (cb === undefined) return promisify(this.update).call(this, id, input)

    const { add, remove } = (input.authors || {})
    this.processAuthors(add, (err, addAuthors) => {
      if (err) return cb(err)

      this.processAuthors(remove, (err, removeAuthors) => {
        if (err) return cb(err)

        if (addAuthors || removeAuthors) {
          input = { ...input, authors: {} }
          if (addAuthors) input.authors.add = addAuthors
          if (removeAuthors) input.authors.remove = removeAuthors
        }

        super.update(id, input, cb)
      })
    })
  }

  /* inheritted */
  // - read
  // - tombstone
  // - list
}
